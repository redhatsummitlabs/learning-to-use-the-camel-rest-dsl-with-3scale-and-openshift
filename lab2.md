# Lab 2 - Add Swagger Docs

Now that you have your camel route deployed it would be nice to have some documentation. 

## Add the Swagger Dependency

First we need to add the swagger dependency to the pom.xml file.  Open the pom.xml file under rest-dsl and around line 49 add the following where it has a TODO

```xml
    <dependency>
      <groupId>org.apache.camel</groupId>
      <artifactId>camel-swagger-java-starter</artifactId>
    </dependency>
```

## Add API Mapping to Your Camel Route

Next we need to tell Camel where to put the swagger docs, a version, and a description for your route. First add the context mapping for where you want the docs to live as well as the api version like so in your SampleCamelRouter.

```java
        restConfiguration()
        	.component("servlet")
    		.bindingMode(RestBindingMode.json)
    		.apiContextPath("api")
            .apiProperty("api.title", "Hello World")
    		.apiProperty("api.version", "1.0.0");
```

Next add your description to your route. 

```java
        rest().description("Service to say Hello to you")
            .get("/hello").produces("text/plain")
            .responseMessage().code(200).message("OK").endResponseMessage()
            .route().routeId("say-hello")
            .to("direct:hello");
```

## Deploy to OpenShift

Return to your terminal and ensure you are in the rest-dsl folder using your project.  Then rerun the mvn fabric8 deploy command. 

```
$ pwd
/projects/learning-to-use-the-camel-rest-dsl-with-3scale-and-openshift/rest-dsl
$ mvn clean fabric8:deploy
```

Once you see success like this, head back to your openshift console at https://master.07c0.summit.opentlc.com/console/project/nameOfYourProject/overview. 

![lab2-deployment-success.png](./images/lab2-deployment-success.png)

Once again click on the camel link. This time append /camel/api to the end of the url to see your swagger docs. 

![lab1-camel-link.png](./images/lab1-camel-link.png)

You should see a page similar to the following. 

![lab2-swagger.png](./images/lab2-swagger.png)

Success! Congrats! This completes lab 2.  You can move on to Lab 3. 

[Go To Lab 3](https://gitlab.com/redhatsummitlabs/learning-to-use-the-camel-rest-dsl-with-3scale-and-openshift/blob/master/lab3.md)


