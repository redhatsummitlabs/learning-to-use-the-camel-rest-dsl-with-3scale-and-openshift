# Extra Credit: Add Docs to 3Scale

From the left hand side menu select 'ActiveDocs'.  Then, from the cooresponding page select 'Create your first spec'. 

![ec-active-docs.png](./images/ec-active-docs.png)

On the next page give your docs an appropriate unique name and description like so. The Name is for display and system name is the artifact name, hence the need for it to not contain spaces or other odd characters. Make sure published is checked.  

In the API JSON Spec paste the swagger you generated back in lab 2.  

![ec-new-docs.png](./images/ec-new-docs.png)

If you need to follow these steps to get the swagger again. 

Head back to your openshift console at https://master.07c0.summit.opentlc.com/console/project/nameOfYourProject/overview. 

Once again click on the camel link and append /camel/api to the end of the url to see your swagger docs. 

![lab1-camel-link.png](./images/lab1-camel-link.png)

You should see a page similar to the following. 

![lab2-swagger.png](./images/lab2-swagger.png)

After creating your active doc, you should be taken to a page like this with your live doc. Check it out!

![ec-live-doc.png](./images/ec-live-doc.png)

Play around!