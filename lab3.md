# Lab 3 - Manage with 3Scale

## Login to the 3Scale Management Console

Return back to the tutorial landing page: https://tutorial-web-app-webapp.apps.07c0.summit.opentlc.com/ . This time select Red Hat 3Scale API Managment Platform from the right hand side. 

![lab3-select-3scale.png](./images/lab3-select-3scale.png)

You will be navigated to a login page.  From there select the authenticate through Red Hat Single Sign-On. 

![lab3-login.png](./images/lab3-login.png)

![lab3-sso.png](./images/lab3-sso.png)

You will land on the 3Scale landing page like this. 

![lab3-3scale-landing.png](./images/lab3-3scale-landing.png)

## Define Your API

For now you can just hit the X in the upper right hand corner. Next, you will see a page like the one below.  Select New API.

![lab3-3scale-admin-console.png](./images/lab3-3scale-admin-console.png)

For learning purposes we will define this API Manually. Name your API whatever you would like such as 'Mary's API', similarly add a System Name, and description such as 'marys-api' and 'summit lab api'. Select Add

![lab3-new-api.png](./images/lab3-new-api.png)

After clicking add you should be navigated to the API's overview page. If not, go to the home page by clicking the Red Hat 3Scale API Management in the upper left hand corner.  Then select your API and finally 'Overview'. 

![lab3-api-landing-page.png](./images/lab3-api-landing-page.png)

## Create an Application Plan

Towards the bottom of your API Overview Page there will be a section for Application Plans.  These plans allow us to personalize things like rate limiting, monetization, and more.  Select Create Application Plan. 

![lab3-create-app-plan.png](./images/lab3-create-app-plan.png)

Give it a personalized name and system name such as 'Mary's App Plan' and 'marys-app-plan'. Leave everything else as default. Click Create Application Plan in the lower right hand corner. 

![lab3-name-app-plan.png](./images/lab3-name-app-plan.png)

Once you create the application plan you need to publish it.  From the Application plan landing page, click publish. 

![lab3-app-plan-landing.png](./images/lab3-app-plan-landing.png)

## Create a New Method

From there select your newly published Application Plan

![lab3-select-your-plan.png](./images/lab3-select-your-plan.png)

If you scroll down on the following page you will see a section showing methods and metrics. Select Define. 

![lab3-methods.png](./images/lab3-methods.png)

Then, frmo the Methods and Metrics landing page select New Method. 

![lab3-methods-landing.png](./images/lab3-methods-landing.png)

From this page give your method a unique name and description like so 

![lab3-name-your-method.png](./images/lab3-name-your-method.png)

Select 'Create Method'

![lab3-create-method.png](./images/lab3-create-method.png)

## Create an Application

From the top Menu where it says API: <Name of your API>, select the drop down and select Audience

![lab3-select-audience.png](./images/lab3-select-audience.png)

You should land on a page like this with your user listed. 

![lab3-audience-landing.png](./images/lab3-audience-landing.png)

Select your account. 

![lab3-select-account.png](./images/lab3-select-account.png)

As you can see on your account page your user is already subscribed to the default Developer App.  We need to subscribe to your new application as well. Select the '1 Application' link in the top breadcrumb area. 

![lab3-account-landing.png](./images/lab3-account-landing.png)

From this next page you can see all the applications your account is subscribed to. So far, just the Developer app.  We need to create a new application for your account is subscribe to based on the API we have.  Select 'Create Application'. 

![lab3-account-application.png](./images/lab3-account-application.png)

Select your application plan that you created earlier. Leave the service plan as default and give you new application a unique name. Select 'Create Application'

![lab3-new-app.png](./images/lab3-new-app.png)

You will land on your new application's landing page.  Success!

![lab3-app-landing.png](./images/lab3-app-landing.png)

## Integrate the API 

To integrate your API with the method, application plan, and application you just created we need to go to the integration section of the 3Scale Management Portal.  From the left hand side menu select Integration --> Configuration. 

![lab3-menu-intg.png](./images/lab3-menu-intg.png)

From the integration landing page select 'add the base url of your API and save the configuration'

![lab3-intg-landing.png](./images/lab3-intg-landing.png)

Now we are on the page to set up all the integration.  Put your camel base url (which you can find from the openshift console) as the base url. 

![lab3-baseurl.png](./images/lab3-baseurl.png)

In addition, update the staging and production url to the following:

staging: hhttps://api-3scale-apicast-staging.apps.07c0.summit.opentlc.com
production: https://api-3scale-apicast-production.apps.07c0.summit.opentlc.com

![lab3-api-gateway.png](./images/lab3-api-gateway.png)

Scroll down and expand Mapping Rules.  Click the pencil on the right hand side to edit the default Get Method. 

![lab3-mapping-rules.png](./images/lab3-mapping-rules.png)

Replace the path with '/camel/hello' and select the method you defined. 

![lab3-camel-mapping.png](./images/lab3-camel-mapping.png)

Scroll down to the Client section and in the test GET request box replace the '/' with '/camel/hello'

![lab3-client-test.png](./images/lab3-client-test.png)

Select Update and Test in Staging

![lab3-update-test.png](./images/lab3-update-test.png)

You should see a green line on the left hand side if everything is successful. 

## Test it out with CURL 

By the client section you will see a curl command such as:

```
curl "https://api-3scale-apicast-staging.apps.07c0.summit.opentlc.com:443/camel/hello?user_key=4bad74255b201bd05e317668ce639c76"
```

Try it out in your codeready terminal to see how it works firsthand. You should get success.  Now try it out without the user_key and notice a failure. 

## Add Rate Limiting

On the left hand side menu select Applications --> Application Plans

![lab3-menu-ap.png](./images/lab3-menu-ap.png)

Select your application plan. 

![lab3-select-ap.png](./images/lab3-select-ap.png)

Scroll down on your application plan landing page to the Metrics, Methods, Limits, and Pricing Rules Section. Click on Limits in the row of your method.  This will expand the limits on that method. Next, select 'New Usage Limit'

![lab3-limits.png](./images/lab3-limits.png)

Check out the different types of usage limits you can create.  In this case we can create one for eternity with the limit of 5. 

![lab3-create-limit.png](./images/lab3-create-limit.png)

Once the limit is created be sure to click 'Update Application Plan'

![lab3-update-ap.png](./images/lab3-update-ap.png)

## Test it out with CURL

Back in your CodeReady Workspace terminal test out curling your API until the limit is hit. When the limit is hit you should see 'Limits exceeded'.  Feel free to play with your limits at this point. 

```
[jboss@workspacebgthbh5nts2u8uhs rest-dsl]$ curl "https://api-3scale-apicast-staging.apps.07c0.summit.opentlc.com:443/camel/hello?user_key=4bad74255b201bd05e317668ce639c76"
"Hello World"
[jboss@workspacebgthbh5nts2u8uhs rest-dsl]$ curl "https://api-3scale-apicast-staging.apps.07c0.summit.opentlc.com:443/camel/hello?user_key=4bad74255b201bd05e317668ce639c76"
"Hello World"
[jboss@workspacebgthbh5nts2u8uhs rest-dsl]$ curl "https://api-3scale-apicast-staging.apps.07c0.summit.opentlc.com:443/camel/hello?user_key=4bad74255b201bd05e317668ce639c76"
"Hello World"
[jboss@workspacebgthbh5nts2u8uhs rest-dsl]$ curl "https://api-3scale-apicast-staging.apps.07c0.summit.opentlc.com:443/camel/hello?user_key=4bad74255b201bd05e317668ce639c76"
"Hello World"
[jboss@workspacebgthbh5nts2u8uhs rest-dsl]$ curl "https://api-3scale-apicast-staging.apps.07c0.summit.opentlc.com:443/camel/hello?user_key=4bad74255b201bd05e317668ce639c76"
Limits exceeded
[jboss@workspacebgthbh5nts2u8uhs rest-dsl]$
```

Congrats on finishing lab 3!  You can now move onto to Extra Credit or play around with your camel route and api management futher. 

[Go To Extra Credit](https://gitlab.com/redhatsummitlabs/learning-to-use-the-camel-rest-dsl-with-3scale-and-openshift/blob/master/extra-credit.md)