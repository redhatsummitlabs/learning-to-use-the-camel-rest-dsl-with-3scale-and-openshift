# Learning to use the Camel Rest DSL with 3Scale and Openshift

## [Lab 0 - Login to Your Environment](/lab0.md)

## [Lab 1 - Create a Camel Route and Deploy to OpenShift](/lab1.md)

## [Lab 2 - Add Swagger Docs](/lab2.md)

## [Lab 3 - Manage With 3Scale](/lab3.md)

## [Extra Credit: Add Docs to 3Scale](/extra-credit.md)
